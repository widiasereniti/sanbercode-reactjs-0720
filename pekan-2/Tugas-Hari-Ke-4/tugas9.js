// SOAL 1
function newFunction(firstName, lastName){
  return {
    fullName(){
      console.log(firstName + " " + lastName)
    }
  }
}

newFunction("William", "Imoh").fullName() 
console.log("\n")

// SOAL 2
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

console.log("\n")

// SOAL 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)