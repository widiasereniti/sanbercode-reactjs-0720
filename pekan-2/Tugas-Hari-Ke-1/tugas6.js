// SOAL 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objDaftarPeserta = {
    nama: "Asep",
    "jenis kelamin": "laki-laki",
    hobi: "baca buku",
    umur: 1992
}
console.log(objDaftarPeserta)
// console.log(objDaftarPeserta["jenis kelamin"])

console.log("\n")
// SOAL 2
// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000 
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000

var fruit = [{nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000}, 
{nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000}, 
{nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000},
{nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}
]
// console.log(fruit)
console.log(fruit[0])

console.log("\n")

// SOAL 3
var dataFilm = []

var newFilm = {
    nama: "Transformers",
    durasi: "2 jam",
    genre: "Action",
    tahun: 2010
}

function addnewFilm(newFilm){
    return dataFilm.push(newFilm)
}

addnewFilm(newFilm)
console.log(dataFilm)

console.log("\n")

// SOAL 4
class Animal {
    constructor(name){
        this.name = name,
        this.legs = 4,
        this.cold_blooded = false
    }
}
// Release 0
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal{
    constructor(name){
        super(name);
        // this.yell = "Auooo";
    }

    yell() {
        return "Auooo";
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
        // this.jump = "hop hop";
    }

    jump(){
        return "hop hop";
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 

console.log("\n")

// SOAL 5
class Clock {
    constructor({template}) {
        var timer;

        function render() {
            var date = new Date();
            var hours = date.getHours();

            if (hours < 10) hours = '0' + hours;

            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;

            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            var output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);

            console.log(output);
        }

        this.stop = function () {
            clearInterval(timer);
        };

        this.start = function () {
            render();
            timer = setInterval(render, 1000);
        };
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  