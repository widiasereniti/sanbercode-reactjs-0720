// SOAL 1
let luas = ""
let keliling = ""

const luasLingkaran = (r) => {
    luas = 3.14 * (r * r)
    return luas
}

const kelilingLingkaran = (d) => {
    keliling = 3.14 * d
    return keliling
}

console.log(luasLingkaran(5))
console.log(kelilingLingkaran(5))
console.log("\n")

// SOAL 2
let kalimat = ""

kata1 = "saya"
kata2 = "adalah"
kata3 = "seorang"
kata4 = "frontend"
kata5 = "developer"

const addWords = (kata1, kata2, kata3, kata4, kata5) => {
    return kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
}
console.log(addWords(kata1, kata2, kata3, kata4, kata5));
console.log("\n")

// SOAL 3
class Book {
    constructor(name, totalPage, price) {
      this.name = name
      this.totalPage = totalPage
      this.price = price
    }

    showBook(){
        return "book: " + this.name + ", total page: " + this.totalPage + ", price: " + this.price
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
      super(name, totalPage, price)
      this.isColorful = isColorful
    }
    
    showComic(){
        if (this.isColorful == true){
            return this.showBook() + ", colorful: " + "yes"
        } else {
            return this.showBook() + ", colorful: " + "no"
        }
    }
}

const checkBooks = new Komik("Akatsuki No Yona", 30, 25000, true);
console.log(checkBooks.showComic());
