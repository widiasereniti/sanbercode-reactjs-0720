import React from 'react';
// import logo from './logo.svg';
// import './App.css';
import './Form.css';

function App() {
  return (
      <form id="card">
        <h1><strong>Form Pembelian Buah</strong></h1>
        <label><strong>Nama Pelanggan</strong></label>
        <input type="text" name="name" />
        <div className="checkbox">
            <label><strong>Daftar Item</strong></label>
            <div>
              <input type="checkbox" id="semangka" name="semangka" value="Semangka" />
              <label> Semangka</label><br/>
              <input type="checkbox" id="jeruk" name="jeruk" value="Jeruk" />
              <label> Jeruk</label><br/>
              <input type="checkbox" id="nanas" name="nanas" value="Nanas" />
              <label> Nanas</label><br/>
              <input type="checkbox" id="salak" name="salak" value="Salak" />
              <label> Salak</label><br/>
              <input type="checkbox" id="anggur" name="anggur" value="Anggur" />
              <label> Anggur</label><br/>
            </div>
        </div>
        <br/>
        <div>
          <button>Kirim</button>
        </div>
      </form>
  );
}

export default App;
