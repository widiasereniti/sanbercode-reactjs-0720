// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var output1 = kataPertama+" "+(kataKedua.charAt(0).toUpperCase()+ kataKedua.slice(1))+" "+kataKetiga+" "+kataKeempat.toUpperCase()
console.log(output1)

//Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var strktPertama = parseInt(kataPertama)
var strktKedua = parseInt(kataKedua)
var strktKetiga = parseInt(kataKetiga)
var strktKeempat = parseInt(kataKeempat)

var output2 = strktKedua+strktKedua+strktKetiga+strktKeempat
console.log(output2)

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 10);
var kataKetiga = kalimat.substr(15, 3);
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substr(25, 6);

// var kataKedua = kalimat.substring(4, 14);
// var kataKetiga = kalimat.substring(15, 19);
// var kataKeempat = kalimat.substring(19, 24);
// var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//Soal 4
var nilai = 75;

if (nilai >= 80){
    console.log("A");
}else if (nilai >= 70 && nilai < 80){
    console.log("B")
}else if (nilai >= 60 && nilai < 70){
    console.log("C")
}else if (nilai >= 50 && nilai < 60){
    console.log("D")
}else {
    console.log("E")
}

// Soal 5

var tanggal = 25;
var bulan = 11;
var tahun = 1996;

switch(bulan){
    case 1:
        console.log("Januari");
        break;
    case 2: 
        console.log("Febuari");
        break;
    case 3:
        console.log("Maret");
        break;
    case 4: 
        console.log("April");
        break;
    case 5:
        console.log("Mei");
        break;
    case 6: 
        console.log("Juni");
        break;
    case 7:
        console.log("Juli");
        break;
    case 8: 
        console.log("Agustus");
        break;
    case 9:
        console.log("September");
        break;
    case 10: 
        console.log("Oktober");
        break;
    case 11: 
        console.log("November");
        break;
    case 12: 
        console.log("Desember");
        break;
    default:
        console.log("Bukan Nama Bulan")
}
