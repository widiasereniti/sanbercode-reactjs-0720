// SOAL 1 while

var pertama = 2

console.log('LOOPING PERTAMA')
while (pertama <= 20){
    console.log(pertama + ' - I love coding')
    pertama += 2
}

console.log('LOOPING KEDUA')
while (pertama > 2){
    pertama -= 2
    console.log(pertama + ' - I will become a frontend developer')
}

console.log("\n")

// SOAL 2 for

for(i = 1; i <= 20; i++) {
    if (i%2 == 0){
        console.log(i + ' - Berkualitas')
    } else {
        if(i%3 == 0){
            console.log(i + ' - I Love Coding')
        }else{
            console.log(i + ' - Santai')
        }
    }
}

console.log("\n")

// SOAL 3 while, for, do while
var output = ''

for(i=1; i<=7; i++){
    output += '#'
    console.log(output)
}

console.log("\n")

// SOAL 4

var kalimat="saya sangat senang belajar javascript"
var splitkal = kalimat.split(' ') 
console.log(splitkal)

console.log("\n")

// SOAL 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort().join("\n")
console.log(sortBuah)