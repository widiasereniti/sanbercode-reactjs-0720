// SOAL 1
function halo(){
    var kata = "Halo Sanbers!" 
    return kata
} 
console.log(halo())
console.log("\n")

// SOAL 2
function kalikan(A, B){
    return A * B
}

var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log("\n")

// SOAL 3
function introduce(name, age, address, hobby){
    var nama = "Nama saya " + name
    var umur = "umur saya " + age + " tahun"
    var alamat = "alamat saya di " + address.charAt(0).toLowerCase()+ address.slice(1)
    var hobi = "dan saya punya hobby yaitu " + hobby
    var output = nama +", "+ umur + ", " + alamat + ", " + hobi + "!"
    return output
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)